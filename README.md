# 批量后台任务处理

#### 介绍

基于.Net Core 3.1 和Quartz.Net ,前端采用Bootstrap, AdminLTE 开发，简单易用的后台批量定时任务处理系统。  

添加任务:  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0312/113205_7aa0b345_603779.png "1.png")
任务列表:  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0312/113217_5f67bbc4_603779.png "2.png")
任务历史:  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0312/113228_0a1c6b24_603779.png "3.png")

#### 软件特点
支持Windows，linux，和Docker 部署。  
支持Cron 表达式设置任务，支持任务暂停，任务状态实时更新，和任务失败邮件通知功能。
支持传入任务参数，任务执行Notes。
内置的sqlite 数据库保存任务和任务执行的历史记录。  
内置的历史记录定期清除功能，内置邮件发送队列，内部集成的Dapper 可以用来处理任务数据。  
内置了2个例子任务,可以参照他们完成其他复杂的任务。


#### 安装教程

docker 安装：  
sudo mkdir /docker_data/dbfile  
sudo git clone https://gitee.com/tm2002/FI.BatchJob FI.Batchjob  
cd FI.Batchjob  
sudo docker build . -t fibatchjob -f Dockerfile  
sudo docker run -d -p 8000:80 -v /docker_data/dbfile:/app/dbfile --name fibatchjob -t fibatchjob   

#### 集成的组件：  

Quartz.Net 处理定时任务  
PubSub 订阅和发布任务状态更新  
MailKit 发送Email  
Microsoft.AspNetCore.SignalR 更新UI状态。  
Dapper 处理数据访问  
MySql.Data 连接Mysql 数据库  

