﻿$(document).ready(function () {
    Ladda.bind('.ladda', { timeout: 2000 });
 
});
function ShowMessage(msg) {
    toastr.options = {
        "positionClass": "toast-top-center"
    };
    toastr.success(msg);
}

function ShowMessageError(msg) {
    toastr.options = {
        "positionClass": "toast-top-center"
    };
    toastr.error(msg);
}