﻿

var popup, dataTable;
var entity = 'Order';
var apiurl = '/Order/' + entity;

$(document).ready(function () {

    $('#grid').DataTable({
        "oLanguage": {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "对不起，没有匹配的数据",
            "sInfo": "第 _START_ - _END_ 条 / 共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有匹配的数据",
            "sInfoFiltered": "(数据表中共 _MAX_ 条记录)",
            "sProcessing": "正在加载中...",
            "sSearch": "全文搜索：",
            "oPaginate": {
                "sFirst": "第一页",
                "sPrevious": " 上一页 ",
                "sNext": " 下一页 ",
                "sLast": " 最后一页 "
            },
        },
        "lengthChange": false,
        "order": [[7, 'desc']],
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [2, 3, 4, 5, 6] }],
        "responsive": true



    });

});





function ShowPopup(url) {
    var modalId = 'modalDefault';
    var modalPlaceholder = $('#' + modalId + ' .modal-dialog .modal-content');
    $.get(url)
        .done(function (response) {
            modalPlaceholder.html(response);
            popup = $('#' + modalId + '').modal({
                keyboard: false,
                backdrop: 'static'
            });
        }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
            if (errorThrown === 'Unauthorized') {
                ShowMessageError("Session 过期，请重新登录");
                setTimeout(function () {
                    window.location.href = "/Account/Login";
                }, 2000);
            } else {
                ShowMessageError(textStatus);
            }
        });
}


function SubmitAddEdit(form) {
    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        var data = $(form).serializeJSON();
        data = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            url: apiurl + '/Save',
            data: data,
            contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    popup.modal('hide');
                    ShowMessage(data.message);
                    setTimeout(function () {
                        window.location.href = "/Order/Order/All";
                    }, 2000);

                } else {
                    ShowMessageError(data.message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (errorThrown === 'Unauthorized') {
                    ShowMessageError("Session 过期，请重新登录");
                    setTimeout(function () {
                        window.location.href = "/Account/Login";
                    }, 2000);
                }
                else {
                    ShowMessageError(textStatus);
                }

            }
        });

    }
    return false;
}
