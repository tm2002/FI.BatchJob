﻿

var popup, dataTable;
var entity = 'Order';
var apiurl = '/Order/' + entity;

$(document).ready(function () {

    $('#grid').DataTable({
        "oLanguage": {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "对不起，没有匹配的数据",
            "sInfo": "第 _START_ - _END_ 条 / 共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有匹配的数据",
            "sInfoFiltered": "(数据表中共 _MAX_ 条记录)",
            "sProcessing": "正在加载中...",
            "sSearch": "全文搜索：",
            "oPaginate": {
                "sFirst": "第一页",
                "sPrevious": " 上一页 ",
                "sNext": " 下一页 ",
                "sLast": " 最后一页 "
            },
        },
        "lengthChange": false,
        "order": [[6, 'desc']],
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [2, 3, 4, 5] }],
        "responsive": true



    });


    

});











