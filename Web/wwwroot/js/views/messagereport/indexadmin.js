﻿var table;
$(document).ready(function () {

    $("#startdate,#enddate").datepicker();

    table = $('#grid').DataTable({
        "oLanguage": {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "对不起，没有匹配的数据",
            "sInfo": "第 _START_ - _END_ 条 / 共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有匹配的数据",
            "sInfoFiltered": "(数据表中共 _MAX_ 条记录)",
            "sProcessing": "正在加载中...",
            "sSearch": "全文搜索：",
            "oPaginate": {
                "sFirst": "第一页",
                "sPrevious": " 上一页 ",
                "sNext": " 下一页 ",
                "sLast": " 最后一页 "
            }
        },
        "responsive": true,
        "lengthChange": true,
        "searching": false,
        "order": [[1, 'desc']],
        //"aoColumnDefs": [{ "bSortable": false, "aTargets": [2, 3, 4, 5, 6] }],
        "serverSide": true,
        ajax: {
            url: '/Message/MessageReport/GetAll',
            type: 'POST',
            async: true,
            //headers: {
            //    "XSRF-TOKEN": document.querySelector('[name="__RequestVerificationToken"]').value
            //},
            data: function (data) {

                return {
                    param: data,
                    startdate: $("#startdate").val(),
                    enddate: $("#enddate").val()
                };
                //return JSON.stringify(data);
            }

        },
        columns: [
            {
                title: "用户",
                data: "userName",
                name: "UserName"
            },
            {
                title: "发送时间",
                data: "sendDate",
                name: "SendDate"
            },
            {
                title: "总数量",
                data: "sendCount",
                name: "SendCount"
            },
            
            {
                title: "成功数量",
                data: "succeedCount",
                name: "SucceedCount"
            },
            {
                title: "失败数量(立即归还)",
                data: "failedCount",
                name: "FailedCount"
            },
            {
                title: "未知数量数量",
                data: "sendingCount",
                name: "SendingCount"
            }
            ,
            {
                title: "未知数量归还(一周内)",
                data: "unknownCount",
                name: "UnknownCount"
            },
            {
                title: "回复数量",
                data: "repliedCount",
                name: "RepliedCount"
                
            },
            {
                title: "成功率",
                data: "succeedRadio",
                name: "SucceedRadio"

            }
        ]



    });


    $("#query").click(function () {
        console.log(table);
        //table.search();
        table.draw();
    });


});

