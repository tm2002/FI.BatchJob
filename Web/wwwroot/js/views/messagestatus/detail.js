﻿var table;

$(document).ready(function () {

    $("#startdate,#enddate").datepicker();

    table = $('#grid').DataTable({
        "oLanguage": {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "对不起，没有匹配的数据",
            "sInfo": "第 _START_ - _END_ 条 / 共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有匹配的数据",
            "sInfoFiltered": "(数据表中共 _MAX_ 条记录)",
            "sProcessing": "正在加载中...",
            "sSearch": "全文搜索：",
            "oPaginate": {
                "sFirst": "第一页",
                "sPrevious": " 上一页 ",
                "sNext": " 下一页 ",
                "sLast": " 最后一页 "
            }
        },
        "responsive": true,
        "lengthChange": true,
        "searching": false,
        "order": [[1, 'asc']],
        //"aoColumnDefs": [{ "bSortable": false, "aTargets": [0] }],
        "serverSide": true,
        ajax: {
            url: '/Message/MessageStatus/GetDetails',
            type: 'POST',
            async: true,
            //headers: {
            //    "XSRF-TOKEN": document.querySelector('[name="__RequestVerificationToken"]').value
            //},
            data: function (data) {

                return {
                    param: data,
                    taskCode: $("#taskCode").val(),
                    telephonenumber: $("#telnumber").val(),
                    status: $("#status").val(),
                    startdate: $("#startdate").val(),
                    enddate: $("#enddate").val()
                };
                //return JSON.stringify(data);
            }

        },

        columns: [
            {
                title: "发送码",
                data: "taskCode",
                name: "TaskCode"
            },
            {
                title: "电话号码",
                data: "phoneNumber",
                name: "PhoneNumber"
            },
            {
                title: "状态",
                data: "status",
                name: "Status",
                render: function (data, type, row) {

                    if (data == "0") {
                        return "发送中";
                    }
                    else if (data == "2") {
                        return "失败(已归还)";
                    }
                    else if (data == "3") {
                        return "未知";
                    } else if (data == "4") {
                        return "未知(已归还)";
                    }
                    else {
                        return "成功";
                    }
                }
            },
            {
                title: "发送时间",
                data: "createdTime",
                name: "CreatedTime",
                render: function (data, type, row) {
                    return window.moment(data).format("YYYY-MM-DD HH:mm:ss");
                }
            }
        ]



    });


    $("#query").click(function () {
        console.log(table);
        //table.search();
        table.draw();
    });


});
