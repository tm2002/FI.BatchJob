﻿var table;
$(document).ready(function () {

    //$('#grid').DataTable({
    //    "oLanguage": {
    //        "sLengthMenu": "每页显示 _MENU_ 条记录",
    //        "sZeroRecords": "对不起，没有匹配的数据",
    //        "sInfo": "第 _START_ - _END_ 条 / 共 _TOTAL_ 条数据",
    //        "sInfoEmpty": "没有匹配的数据",
    //        "sInfoFiltered": "(数据表中共 _MAX_ 条记录)",
    //        "sProcessing": "正在加载中...",
    //        "sSearch": "全文搜索：",
    //        "oPaginate": {
    //            "sFirst": "第一页",
    //            "sPrevious": " 上一页 ",
    //            "sNext": " 下一页 ",
    //            "sLast": " 最后一页 "
    //        },
    //    },
    //    "lengthChange": false,
    //    "order": [[0, 'asc']],
    //    "aoColumnDefs": [{ "bSortable": false, "aTargets": [2, 3, 4, 6] }],
    //    "responsive": true



    //});

    $("#startdate,#enddate").datepicker();

    table = $('#grid').DataTable({
        "oLanguage": {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "对不起，没有匹配的数据",
            "sInfo": "第 _START_ - _END_ 条 / 共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有匹配的数据",
            "sInfoFiltered": "(数据表中共 _MAX_ 条记录)",
            "sProcessing": "正在加载中...",
            "sSearch": "全文搜索：",
            "oPaginate": {
                "sFirst": "第一页",
                "sPrevious": " 上一页 ",
                "sNext": " 下一页 ",
                "sLast": " 最后一页 "
            }
        },
        "responsive": true,
        "lengthChange": true,
        "searching": false,
        "order": [[5, 'desc']],
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [2, 3, 4, 6] }],
        "serverSide": true,
        ajax: {
            url: '/Message/MessageStatus/GetAll',
            type: 'POST',
            async: true,
            //headers: {
            //    "XSRF-TOKEN": document.querySelector('[name="__RequestVerificationToken"]').value
            //},
            data: function (data) {

                return {
                    param: data,
                    taskCode: $("#taskcode").val(),
                    startdate: $("#startdate").val(),
                    enddate: $("#enddate").val()
                };
                //return JSON.stringify(data);
            }

        },

        columns: [
            {
                title: "模板名称",
                data: "templateName",
                name: "TemplateName"
            },
            {
                title: "模板内容",
                data: "templateContent",
                name: "TemplateContent"
            },
            {
                title: "电话号码数量",
                data: "phoneNumberQuantity",
                name: "PhoneNumberQuantity"
            },
            {
                title: "发送码",
                data: "taskCode",
                name: "TaskCode"
            },

            {
                title: "发送类型",
                data: "sendType",
                name: "SendType",
                render: function (data, type, row) {

                    if (data == "0") {
                        return "立即发送";
                    }

                    else {
                        return "定时发送";
                    }
                }
            },
            {
                title: "发送时间",
                data: "sendTime",
                name: "SendTime",
                render: function (data, type, row) {

                    console.log(data);
                    return window.moment(data).format("YYYY-MM-DD HH:mm:ss");
                }
            },
            {
                title: "",
                data: null,
                name: null,
                render: function (data, type, row) {
                    console.log("ddddd"+row.taskCode);
                    return "<a class='btn btn-default btn-xs' style='margin-left: 2px' href='/Message/MessageStatus/Detail?taskcode=" + row.taskCode + " '>详细</a>";
                }
            }
        ]



    });


    $("#query").click(function () {
        console.log(table);
        //table.search();
        table.draw();
    });


});
