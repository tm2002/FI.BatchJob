﻿

var popup, dataTable;
var entity = 'MessageNumberClassify';
var apiurl = '/Message/' + entity;

$(document).ready(function () {


    $(function () {

        $("#classify").click(function () {
            $.ajax({
                type: 'POST',
                url: '/Message/MessageNumberClassify/Classify',
                data: { 'numbers': $("#TelephoneNumbers").val() },
                //contentType: 'application/json',
                success: function (data) {
                    if (data.success) {

                        ShowMessage(data.message);
                        $("#YiDongNumbers").val(data.yidong);
                        $("#yidongcount").text(data.yidongcount);
                        $("#LianTongNumbers").val(data.liantong);
                        $("#liantongcount").text(data.liantongcount);
                        $("#DianXinNumbers").val(data.dianxin);
                        $("#dianxincount").text(data.dianxincount);

                    } else {
                        ShowMessageError(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (errorThrown === 'Unauthorized') {
                        ShowMessageError("Session 过期，请重新登录");
                        setTimeout(function () {
                            window.location.href = "/Account/Login";
                        }, 2000);
                    }
                    else {
                        ShowMessageError(textStatus);
                    }

                }
            });

        });

        $("#upload").click(function () {

            var fileUpload = $("#files").get(0);
            var files = fileUpload.files;
            var data = new FormData();
            for (var i = 0; i < files.length; i++) {
                data.append(files[i].name, files[i]);
            }

            $.ajax({
                type: "POST",
                url: "/Message/MessageNumberClassify/Upload",
                contentType: false,
                processData: false,
                data: data,
                success: function (data) {

                    if (data.success) {

                        $("#TelephoneNumbers").val(data.data);

                        ShowMessage(data.message);

                    } else {
                        ShowMessageError(data.message);

                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (errorThrown === 'Unauthorized') {
                        ShowMessageError("Session 过期，请重新登录");
                        setTimeout(function () {
                            window.location.href = "/Account/Login";
                        }, 2000);
                    }
                    else {
                        ShowMessageError(textStatus);
                    }
                    
                }
            });


        });

        $("#save").click(function () {

            if ($("#YiDongNumbers").val() || $("#LianTongNumbers").val() || $("#DianXinNumbers").val()) {

                $.ajax({
                    type: 'POST',
                    url: '/Message/MessageNumberClassify/Save',
                    data: {
                        'YiDongNumbers': $("#YiDongNumbers").val(),
                        'LianTongNumbers': $("#LianTongNumbers").val(),
                        'DianXinNumbers': $("#DianXinNumbers").val()

                    },

                    success: function (data) {
                        if (data.success) {

                            ShowMessage(data.message);
                            setTimeout(function () {
                                window.location.href = "/Message/MessageNumberClassify/Index";
                            }, 2000);
                            


                        } else {
                            ShowMessageError(data.message);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        if (errorThrown === 'Unauthorized') {
                            ShowMessageError("Session 过期，请重新登录");
                            setTimeout(function () {
                                window.location.href = "/Account/Login";
                            }, 2000);
                        }
                        else {
                            ShowMessageError(textStatus);
                        }

                    }
                });
            }
            else {
                ShowMessageError("没有号码可以保存。");
            }
        });


    });


});




