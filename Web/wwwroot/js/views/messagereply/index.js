﻿
$(document).ready(function () {

    //$('#grid').DataTable({
    //    "oLanguage": {
    //        "sLengthMenu": "每页显示 _MENU_ 条记录",
    //        "sZeroRecords": "对不起，没有匹配的数据",
    //        "sInfo": "第 _START_ - _END_ 条 / 共 _TOTAL_ 条数据",
    //        "sInfoEmpty": "没有匹配的数据",
    //        "sInfoFiltered": "(数据表中共 _MAX_ 条记录)",
    //        "sProcessing": "正在加载中...",
    //        "sSearch": "全文搜索：",
    //        "oPaginate": {
    //            "sFirst": "第一页",
    //            "sPrevious": " 上一页 ",
    //            "sNext": " 下一页 ",
    //            "sLast": " 最后一页 "
    //        },
    //    },
    //    "lengthChange": false,
    //    "order": [[0, 'asc']],
    //    "aoColumnDefs": [{ "bSortable": false, "aTargets": [2, 3, 4,5] }],
    //    "responsive": true



    //});

    $("#startdate,#enddate").datepicker();

    table = $('#grid').DataTable({
        "oLanguage": {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "对不起，没有匹配的数据",
            "sInfo": "第 _START_ - _END_ 条 / 共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有匹配的数据",
            "sInfoFiltered": "(数据表中共 _MAX_ 条记录)",
            "sProcessing": "正在加载中...",
            "sSearch": "全文搜索：",
            "oPaginate": {
                "sFirst": "第一页",
                "sPrevious": " 上一页 ",
                "sNext": " 下一页 ",
                "sLast": " 最后一页 "
            }
        },
        "responsive": true,
        "lengthChange": true,
        "searching": false,
        "order": [[3, 'desc']],
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [0, 1, 2] }],
        "serverSide": true,
        ajax: {
            url: '/Message/MessageReply/GetAll',
            type: 'POST',
            async: true,
            //headers: {
            //    "XSRF-TOKEN": document.querySelector('[name="__RequestVerificationToken"]').value
            //},
            data: function (data) {

                return {
                    param: data,
                    startdate: $("#startdate").val(),
                    enddate: $("#enddate").val()
                };
                //return JSON.stringify(data);
            }

        },

        columns: [
            {
                title: "电话号码",
                data: "phoneNumber",
                name: "PhoneNumber",
                render: function (data, type, row) {
                    return "<a href='tel:'" + data + ">" + data + "</a>";
                }
            },
            {
                title: "回复内容",
                data: "reply",
                name: "Reply"
            },
            {
                title: "回复时间",
                data: "replyTime",
                name: "ReplyTime",
                render: function (data, type, row) {

                    console.log(data);
                    return window.moment(data).format("YYYY-MM-DD HH:mm:ss");
                }
            },
            {
                title: "发送码",
                data: "taskCode",
                name: "TaskCode"
            },
            {
                title: "",
                data: null,
                name: null,
                render: function (data, type, row) {

                    return "<a class='btn btn-default btn-xs' style='margin-left: 2px' href='/Message/MessageReply/AddContact?number=" + row.phoneNumber + " '>添加到联系人</a>";
                }
            }
            ,
            {
                title: "",
                data: null,
                name: null,
                render: function (data, type, row) {
                  
                    return "<a class='btn btn-default btn-xs' style='margin-left: 2px' href='javascript:void' onclick=Delete(" + row.id + ") '>删除</a>";
                }
            }
        ]


    });


    $("#query").click(function () {
       
       
        table.draw();
    });

});

function Delete(id) {
        swal({
            title: "删除",
            text: "确定删除?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#dd4b39",
            confirmButtonText: "是的，删除!",
            cancelButtonText: "取消",
            closeOnConfirm: true
        }, function () {
            $.ajax({
                type: 'POST',
                url: '/Message/MessageReply/Delete?id=' + id,
                success: function (data) {
                    if (data.success) {
                        ShowMessage(data.message);
                        window.location.href = "/Message/MessageReply/Index";

                    } else {
                        ShowMessageError(data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (errorThrown === 'Unauthorized') {
                        ShowMessageError("Session 过期，请重新登录");
                        setTimeout(function () {
                            window.location.href = "/Account/Login";
                        }, 2000);
                    }
                    else {
                        ShowMessageError(textStatus);
                    }

                }
            });
        });

    }

