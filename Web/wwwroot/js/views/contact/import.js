﻿

var popup, dataTable;
var entity = 'Contact';
var apiurl = '/Contact/' + entity;

$(document).ready(function () {

    $("#upload").click(function () {

        var fileUpload = $("#files").get(0);
        var files = fileUpload.files;
        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }

        $.ajax({
            type: "POST",
            url: apiurl + "/UploadAsync",
            contentType: false,
            processData: false,
            data: data,
            success: function (data) {

                if (data.success) {

                    $("#list").html(data.data);

                    ShowMessage(data.message);

                } else {
                    ShowMessageError(data.message);

                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (errorThrown === 'Unauthorized') {
                    ShowMessageError("Session 过期，请重新登录");
                    setTimeout(function () {
                        window.location.href = "/Account/Login";
                    }, 2000);
                } else {
                    ShowMessageError(textStatus);
                }


            }
        });


    });







    //$('#grid').DataTable({
    //    "oLanguage": {
    //        "sLengthMenu": "每页显示 _MENU_ 条记录",
    //        "sZeroRecords": "对不起，没有匹配的数据",
    //        "sInfo": "第 _START_ - _END_ 条 / 共 _TOTAL_ 条数据",
    //        "sInfoEmpty": "没有匹配的数据",
    //        "sInfoFiltered": "(数据表中共 _MAX_ 条记录)",
    //        "sProcessing": "正在加载中...",
    //        "sSearch": "全文搜索：",
    //        "oPaginate": {
    //            "sFirst": "第一页",
    //            "sPrevious": " 上一页 ",
    //            "sNext": " 下一页 ",
    //            "sLast": " 最后一页 "
    //        },
    //    },
    //    "lengthChange": false,
    //    "order": [[0, 'asc']],
    //    "aoColumnDefs": [{ "bSortable": false, "aTargets": [3, 4] }],
    //    "responsive": true



    //});




});

function ShowPopup(url) {
    var modalId = 'modalDefault';
    var modalPlaceholder = $('#' + modalId + ' .modal-dialog .modal-content');
    $.get(url)
        .done(function (response) {
            modalPlaceholder.html(response);
            popup = $('#' + modalId + '').modal({
                keyboard: false,
                backdrop: 'static'
            });
        }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
            if (errorThrown === 'Unauthorized') {
                ShowMessageError("Session 过期，请重新登录");
                setTimeout(function () {
                    window.location.href = "/Account/Login";
                }, 2000);
            } else {
                ShowMessageError(textStatus);
            }
        });
}


function SubmitAddEdit(form) {
    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        var data = $(form).serializeJSON();
        data = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            url: apiurl + '/Save',
            data: data,
            contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    popup.modal('hide');
                    ShowMessage(data.message);
                    setTimeout(function () {
                        window.location.href = "/Contact/Contact";
                    }, 2000);

                } else {
                    ShowMessageError(data.message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (errorThrown === 'Unauthorized') {
                    ShowMessageError("Session 过期，请重新登录");
                    setTimeout(function () {
                        window.location.href = "/Account/Login";
                    }, 2000);
                } else {
                    ShowMessageError(textStatus);
                }


            }
        });

    }
    return false;
}

function Delete(id,obj) {
    var that = obj;
    swal({
        title: "删除",
        text: "确定删除?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dd4b39",
        confirmButtonText: "是的，删除!",
        cancelButtonText:"取消",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            type: 'POST',
            url: apiurl + '/Delete?id=' + id,
            success: function (data) {
                if (data.success) {

                    $(that).parent().parent().remove();

                    ShowMessage(data.message);
                    
                } else {
                    ShowMessageError(data.message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (errorThrown === 'Unauthorized') {
                    ShowMessageError("Session 过期，请重新登录");
                    setTimeout(function () {
                        window.location.href = "/Account/Login";
                    }, 2000);
                }
                else {
                    ShowMessageError(textStatus);
                }

            }
        });
    });


}




