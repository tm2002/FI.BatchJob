﻿

var popup, dataTable;
var entity = 'Project';
var apiurl = '/Project/' + entity;

$(document).ready(function () {

    dataTable=  $('#grid').DataTable({
        "oLanguage": {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "对不起，没有匹配的数据",
            "sInfo": "第 _START_ - _END_ 条 / 共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有匹配的数据",
            "sInfoFiltered": "(数据表中共 _MAX_ 条记录)",
            "sProcessing": "正在加载中...",
            "sSearch": "全文搜索：",
            "oPaginate": {
                "sFirst": "第一页",
                "sPrevious": " 上一页 ",
                "sNext": " 下一页 ",
                "sLast": " 最后一页 "
            },
        },
        "lengthChange": false,
        "order": [[1, 'asc']],
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [2, 3] }],
        "responsive": true



    });


    

});




function ShowPopup(url) {
    var modalId = 'modalDefault';
    var modalPlaceholder = $('#' + modalId + ' .modal-dialog .modal-content');
    $.get(url)
        .done(function (response) {
            modalPlaceholder.html(response);
            popup = $('#' + modalId + '').modal({
                keyboard: false,
                backdrop: 'static'
            });
        }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
            if (errorThrown === 'Unauthorized') {
                ShowMessageError("Session 过期，请重新登录");
                setTimeout(function () {
                    window.location.href = "/Account/Login";
                }, 2000);
            } else {
                ShowMessageError(textStatus);
            }
        });
}


function SubmitAdd(form) {
    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        var data = $(form).serializeJSON();
        data = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            url: apiurl+"/Save",
            data: data,
            contentType: 'application/json',
            success: function (data) {
                if (data.success) {
                    popup.modal('hide');
                    ShowMessage(data.message);
                    window.location.href = "/Project/Project";
                } else {
                    ShowMessageError(data.message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (errorThrown === 'Unauthorized') {
                    ShowMessageError("Session 过期，请重新登录");
                    setTimeout(function () {
                        window.location.href = "/Account/Login";
                    }, 2000);
                }
                else {
                    ShowMessageError(textStatus);
                }

            }
        });

    }
    return false;
}

function Delete(id) {
    swal({
        title: "Are you sure want to Delete?",
        text: "You will not be able to restore the data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dd4b39",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            type: 'DELETE',
            url: apiurl + '/Delete/' + id,
            success: function (data) {
                if (data.success) {
                    ShowMessage(data.message);
                    window.location.href = "/Project/Project";
                } else {
                    ShowMessageError(data.message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (errorThrown === 'Unauthorized') {
                    ShowMessageError("Session 过期，请重新登录");
                    setTimeout(function () {
                        window.location.href = "/Account/Login";
                    }, 2000);
                }
                else {
                    ShowMessageError(textStatus);
                }

            }
        });
    });


}







