﻿

var popup, dataTable;
var entity = 'MessageSend';
var apiurl = '/Message/' + entity;

$(document).ready(function () {

    $("#upload").click(function () {

        var fileUpload = $("#files").get(0);
        var files = fileUpload.files;
        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }

        $.ajax({
            type: "POST",
            url: apiurl + "/Upload",
            contentType: false,
            processData: false,
            data: data,
            success: function (data) {

                if (data.success) {

                    $("#TelephoneNumbers").val(data.data);

                    ShowMessage(data.message);

                } else {
                    ShowMessageError(data.message);

                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (errorThrown === 'Unauthorized') {
                    ShowMessageError("Session 过期，请重新登录");
                    setTimeout(function () {
                        window.location.href = "/Account/Login";
                    }, 2000);
                }
                else {
                    ShowMessageError(textStatus);
                }

            }
        });


    });

    $("#send").click(function () {
        $("#form").submit();
    });



    $("#load").click(function () {

        var modalId = 'modalDefault';
        var modalPlaceholder = $('#' + modalId + ' .modal-dialog .modal-content');
        $.get(apiurl + '/Load')
            .done(function (response) {
                modalPlaceholder.html(response);
                popup = $('#' + modalId + '').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            });
    });


    //$("#import").on("click", function () {
    //    console.log('ddddddddddddddddd');
    //    var allnums = '';
    //    if ($("#ydtxt").val()) {
    //        allnums = allnums + $("#ydtxt").val() + '\r\n';
    //    }
    //    if ($("#lttxt").val()) {
    //        allnums = allnums + $("#lttxt").val() + '\r\n';
    //    }
    //    if ($("#dxtxt").val()) {
    //        allnums = allnums + $("#dxtxt").val() + '\r\n';
    //    }

    //    $("#TelephoneNumbers").val(allnums);


    //});


});

function SubmitForm(form) {

    $.ladda('stopAll');
    const btn = $('#send').ladda();

    $.validator.unobtrusive.parse(form);
    if ($(form).valid()) {
        var data = $(form).serializeJSON();
        data = JSON.stringify(data);
        $.ajax({
            type: 'POST',
            url: apiurl + '/Send',
            data: data,
            contentType: 'application/json',
            success: function (data) {

                $('#send').ladda('stop');

                if (data.success) {
                    
                    ShowMessage(data.message);
                    swal({
                        title: "",
                        text: data.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#3c8dbc",
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        closeOnConfirm: true
                    }, function () {

                        if (data.scheduled) {
                            window.location.href = "/Message/MessageScheduleStatus/Index";
                        }
                        else {
                            window.location.href = "/Message/MessageStatus/Index";
                        }
                    });

                } else {
                    swal({
                        title: "",
                        text: data.message,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#3c8dbc",
                        confirmButtonText: "确定",
                        cancelButtonText: "取消",
                        closeOnConfirm: true
                    }, function () {

                        
                    });
                   // ShowMessageError(data.message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('#send').ladda('stop');
                if (errorThrown === 'Unauthorized') {
                    ShowMessageError("Session 过期，请重新登录");
                    setTimeout(function () {
                        window.location.href = "/Account/Login";
                    }, 2000);
                }
                else {
                    ShowMessageError(textStatus);
                }

            },
            beforeSend: function () {
                

                // Start loading
                btn.ladda('start');
            }
        });

    }
    return false;
}







