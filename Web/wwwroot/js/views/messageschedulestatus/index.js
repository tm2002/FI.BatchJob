﻿var table;
$(document).ready(function () {

    //$('#grid').DataTable({
    //    "oLanguage": {
    //        "sLengthMenu": "每页显示 _MENU_ 条记录",
    //        "sZeroRecords": "对不起，没有匹配的数据",
    //        "sInfo": "第 _START_ - _END_ 条 / 共 _TOTAL_ 条数据",
    //        "sInfoEmpty": "没有匹配的数据",
    //        "sInfoFiltered": "(数据表中共 _MAX_ 条记录)",
    //        "sProcessing": "正在加载中...",
    //        "sSearch": "全文搜索：",
    //        "oPaginate": {
    //            "sFirst": "第一页",
    //            "sPrevious": " 上一页 ",
    //            "sNext": " 下一页 ",
    //            "sLast": " 最后一页 "
    //        },
    //    },
    //    "lengthChange": false,
    //    "order": [[0, 'asc']],
    //    "aoColumnDefs": [{ "bSortable": false, "aTargets": [1,2, 3, 8] }],
    //    "responsive": true
    //});


    $("#startdate,#enddate").datepicker();

    table = $('#grid').DataTable({
        "oLanguage": {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "对不起，没有匹配的数据",
            "sInfo": "第 _START_ - _END_ 条 / 共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有匹配的数据",
            "sInfoFiltered": "(数据表中共 _MAX_ 条记录)",
            "sProcessing": "正在加载中...",
            "sSearch": "全文搜索：",
            "oPaginate": {
                "sFirst": "第一页",
                "sPrevious": " 上一页 ",
                "sNext": " 下一页 ",
                "sLast": " 最后一页 "
            }
        },
        "responsive": true,
        "lengthChange": true,
        "searching": false,
        "order": [[3, 'desc']],
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [1, 2, 7] }],
        "serverSide": true,
        ajax: {
            url: '/Message/MessageScheduleStatus/GetAll',
            type: 'POST',
            async: true,
            //headers: {
            //    "XSRF-TOKEN": document.querySelector('[name="__RequestVerificationToken"]').value
            //},
            data: function (data) {

                return {
                    param: data,
                    startdate: $("#startdate").val(),
                    enddate: $("#enddate").val()
                };
                //return JSON.stringify(data);
            }

        },

        columns: [
            {
                title: "模板名称",
                data: "name",
                name: "Name"
            },
            {
                title: "模板内容",
                data: "content",
                name: "Content"
            },
            {
                title: "拟发送的电话号码数量",
                data: "length",
                name: "Length"
            },
            {
                title: "设定时间",
                data: "scheduledTime",
                name: "ScheduledTime",
                render: function (data, type, row) {
                    return window.moment(data).format("YYYY-MM-DD HH:mm:ss");
                }
            },
            {
                title: "开始时间",
                data: "beginTime",
                name: "BeginTime",
                render: function (data, type, row) {
                    return window.moment(data).format("YYYY-MM-DD HH:mm:ss");
                }
            },
            {
                title: "结束时间",
                data: "endTime",
                name: "EndTime",
                render: function (data, type, row) {
                    return window.moment(data).format("YYYY-MM-DD HH:mm:ss");
                }
            },
            {
                title: "状态",
                data: "status",
                name: "Status",
                render: function (data, type, row) {

                    if (data == "0") {
                        return "等待中";
                    }
                    if (data == "1") {
                        return "成功";
                    }
                    else {
                        return "失败";
                    }
                }
            },
            {
                title: "处理结果",
                data: "processResult",
                name: "ProcessResult"
            }
        ]



    });


    $("#query").click(function () {
        console.log(table);
        //table.search();
        table.draw();
    });


});

