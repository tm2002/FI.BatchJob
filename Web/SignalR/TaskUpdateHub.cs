﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace FI.BatchJob.Web.SignalR
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.SignalR.Hub" />
    public class TaskUpdateHub : Hub
    {
        public TaskUpdateHub()
        {

        }

        /// <summary>
        /// Sends the message.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="message">The message.</param>
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);

        }

    }
}
