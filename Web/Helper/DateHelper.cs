﻿using System;

namespace FI.BatchJob.Web.Helper
{
    public static class DateHelper
    {

        /// <summary>
        /// 将日期转换成人性化的显示
        /// </summary>
        /// <param name="date">当前时间</param>
        /// <returns></returns>
        public static string ToPrettyDate(this DateTime date)
        {
            var timeSince = DateTime.Now.Subtract(date);
            //if (timeSince.TotalMilliseconds < 1) return "还未到";
            if (timeSince.TotalMinutes < 1) return "刚刚";
            if (timeSince.TotalMinutes < 2) return "1 分钟前";
            if (timeSince.TotalMinutes < 60) return string.Format("{0} 分钟前", timeSince.Minutes);
            if (timeSince.TotalMinutes < 120) return "1 小时前";
            if (timeSince.TotalHours < 24) return string.Format("{0} 小时前", timeSince.Hours);
            if (timeSince.TotalDays == 1) return "昨天";
            if (timeSince.TotalDays < 7) return string.Format("{0} 天前", timeSince.Days);
            if (timeSince.TotalDays < 14) return "1 周前";
            if (timeSince.TotalDays < 21) return "2 周前";
            if (timeSince.TotalDays < 28) return "3 周前";
            if (timeSince.TotalDays < 60) return "1个月前";
            if (timeSince.TotalDays < 365) return string.Format("{0} 个月前", Math.Round(timeSince.TotalDays / 30));
            if (timeSince.TotalDays < 730) return "去年";
            return string.Format("{0} 年前", Math.Round(timeSince.TotalDays / 365));
        }
    }
}
