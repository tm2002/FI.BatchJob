﻿using System;
using System.Reflection;

namespace FI.BatchJob.Web.Helper
{
    /// <summary>
    /// Type相关助手类
    /// </summary>
    public class TypeHelper
    {
        /// <summary>
        /// 反射获取类信息
        /// </summary>
        /// <param name="assemblyName">程序集名称</param>
        /// <param name="className">类名</param>
        /// <returns></returns>
        public static Type BuildTypeFromAssembly(string assemblyName, string className)
        {

            return Assembly.Load(new AssemblyName(assemblyName)).GetType(className);
        }
        /// <summary>
        /// 反射获取类信息
        /// </summary>
        /// <param name="assemblyFile">程序集文件</param>
        /// <param name="className">类名</param>
        /// <returns></returns>
        public static Type BuildTypeFromAssemblyFile(string assemblyFile, string className)
        {
            var assembly = Assembly.LoadFrom(assemblyFile);

            return assembly.GetType(className);
        }
    }
}
