﻿using System.ComponentModel.DataAnnotations;

namespace FI.BatchJob.Web.Models.AccountViewModels
{
    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "必填")]
        [Display(Name = "用户名")]

        public string UserName { get; set; }

        [Required(ErrorMessage = "必填")]
        [StringLength(100, ErrorMessage = "{0}至少{2}位.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "新密码")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "确认密码")]
        [Compare("Password", ErrorMessage = "两次输入的密码不匹配")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "必填")]
        [Display(Name = "验证码")]
        public string ValidationCode { get; set; }
    }
}
