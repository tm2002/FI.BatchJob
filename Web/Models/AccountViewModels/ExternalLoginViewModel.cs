using System.ComponentModel.DataAnnotations;

namespace FI.BatchJob.Web.Models.AccountViewModels
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
