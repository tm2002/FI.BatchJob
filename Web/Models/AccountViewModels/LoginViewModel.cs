﻿using System.ComponentModel.DataAnnotations;

namespace FI.BatchJob.Web.Models.AccountViewModels
{
    public class LoginViewModel
    {


        [Required(ErrorMessage = "必填")]
        [Display(Name = "用户名")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "必填")]
        [DataType(DataType.Password)]
        [Display(Name = "密码")]
        public string Password { get; set; }

        [Display(Name = "记住我?")]
        public bool RememberMe { get; set; }
    }
}
