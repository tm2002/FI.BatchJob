﻿using System.ComponentModel.DataAnnotations;

namespace FI.BatchJob.Web.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "必填")]
        [Display(Name = "用户名")]
        public string UserName { get; set; }

        public string StatusMessage { get; set; }
    }
}
