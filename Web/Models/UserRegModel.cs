﻿using System.ComponentModel.DataAnnotations;

namespace FI.BatchJob.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class UserRegModel
    {
        [Required(ErrorMessage = "必填")]
        [EmailAddress]

        public string Email { get; set; }

        [Required(ErrorMessage = "必填")]
        [StringLength(100, ErrorMessage = "{0}至少{2}位.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "密码")]
        public string Password { get; set; }



        public string Id { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UserChangePasswordModel
    {


        [Required(ErrorMessage = "必填")]
        [StringLength(100, ErrorMessage = "{0}至少{2}位.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "密码")]
        public string Password { get; set; }



        public string Id { get; set; }
    }
}
