﻿namespace FI.BatchJob.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public enum MessageCode
    {
        /// <summary>
        /// 请求超时
        /// </summary>
        ReuestTimeout = 996,
        /// <summary>
        /// 应用程序异常
        /// </summary>
        ServiceException = 997,
        /// <summary>
        /// 非法请求
        /// </summary>
        BadRequest = 998,
        /// <summary>
        /// 未知错误
        /// </summary>
        Unknown = 999,
        /// <summary>
        /// 请求成功
        /// </summary>
        Success = 1000,
        /// <summary>
        /// 失败
        /// </summary>
        Failure = 5005,
        /// <summary>
        /// 存在相同
        /// </summary>
        IsSameExist = 5006,
        /// <summary>
        /// Cron 错误
        /// </summary>
        CronFormatFault = 5007,
        /// <summary>
        /// Task 不存在
        /// </summary>
        TaskMissing = 5008
    }
}
