﻿namespace FI.BatchJob.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class NameGroupModel
    {
        public string Name { get; set; }
        public string Group { get; set; }
    }
}
