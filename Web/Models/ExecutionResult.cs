﻿using System;

namespace FI.BatchJob.Web.Models
{
    /// <summary>
    /// 通用数据返回格式
    /// </summary>
    public class ExecutionResult
    {
        /// <summary>
        /// 状态码
        /// </summary>
        public MessageCode Code { get; set; }
        /// <summary>
        /// 消息
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 数据
        /// </summary>
        public object Data { get; set; }
        /// <summary>
        /// 异常
        /// </summary>
        public Exception exception { get; set; }
    }



}
