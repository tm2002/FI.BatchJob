﻿using System;

namespace FI.BatchJob.Web.Extensions
{
    public static class ServiceLocator
    {
        public static IServiceProvider Instance { get; set; }
    }
}
