﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace FI.BatchJob.Web.Extensions
{
    public static class DistributedCacheExtensions
    {
        public static void Set<T>(this IDistributedCache session, string key, T value, DistributedCacheEntryOptions options)
        {
            session.SetString(key, JsonConvert.SerializeObject(value), options);
        }
        public static void Set<T>(this IDistributedCache session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }
        public static T Get<T>(this IDistributedCache session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) :
                JsonConvert.DeserializeObject<T>(value);
        }



    }
}
