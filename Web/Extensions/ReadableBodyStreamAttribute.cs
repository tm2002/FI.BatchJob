﻿using Microsoft.AspNetCore.Http;

using Microsoft.AspNetCore.Mvc.Filters;

namespace FI.BatchJob.Web.Extensions
{
    public class ReadableBodyStreamAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            context.HttpContext.Request.EnableBuffering();
            base.OnActionExecuting(context);
        }
    }
}
