﻿using FI.BatchJob.Web.Data.Entitities;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;

namespace FI.BatchJob.Web.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public class EmailQueue
    {
        public static EmailQueue Instance = new EmailQueue();
        private EmailQueue()
        {

        }


        /// <summary>
        /// The work items
        /// </summary>
        private ConcurrentQueue<ExceptionMail> _workItems = new ConcurrentQueue<ExceptionMail>();
        private SemaphoreSlim _signal = new SemaphoreSlim(0);

        public void Enqueue(ExceptionMail workItem)
        {
            if (workItem == null)
            {
                throw new ArgumentNullException(nameof(workItem));
            }

            _workItems.Enqueue(workItem);
            _signal.Release();
        }

        public ExceptionMail DequeueAsync()
        {
            _signal.Wait();
            _workItems.TryDequeue(out var workItem);

            return workItem;
        }

        public int EmailCount()
        {
            return _workItems.Count();
        }
    }
}
