﻿using FI.BatchJob.Web.Data.Entitities;

namespace FI.BatchJob.Web.Services
{
    public interface IEmailSender
    {
        /// <summary>
        /// Sends the mail.
        /// </summary>
        /// <param name="emailTitle">The email title.</param>
        /// <param name="emailBody">The email body.</param>
        /// <param name="config">The configuration.</param>
        /// <returns></returns>
        bool SendMail(string emailTitle, string emailBody, MailConfig config);
    }
}
