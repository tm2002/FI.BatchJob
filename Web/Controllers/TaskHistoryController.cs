﻿using FI.BatchJob.Web.Data;
using FI.BatchJob.Web.Data.Entities;
using FI.BatchJob.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FI.BatchJob.Web.Controllers
{

    /// <summary>
    /// Task history controller 
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Authorize]
    public class TaskHistoryController : Controller
    {
        private ApplicationDbContext _dbContext;

        public TaskHistoryController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;

        }
        /// <summary>
        /// List with the specified namegroup.
        /// </summary>
        /// <param name="namegroup">The namegroup.</param>
        /// <returns></returns>
        public IActionResult Index(string namegroup)
        {
            if (!string.IsNullOrEmpty(namegroup))
            {
                var strs = HttpUtility.UrlDecode(namegroup).Split('-');

                if (strs.Length < 2)
                {
                    return View();
                }
                else
                {
                    NameGroupModel model = new NameGroupModel
                    {
                        Name = strs[1],
                        Group = strs[0]
                    };

                    return View(model);
                }
            }
            else
            {
                return View();
            }


        }


        /// <summary>
        /// Gets the task histories.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="length">The length.</param>
        /// <param name="name">The name.</param>
        /// <param name="group">The group.</param>
        /// <returns></returns>
        public IActionResult GetTaskHistories(int start, int length, string name, string group)
        {
            List<ScheduleHistory> list;

            int count = 0;

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(group))
            {

                count = _dbContext.ScheduleHistories.Count();

                list = _dbContext.ScheduleHistories
                    .OrderByDescending(n => n.BeginTime).Skip(start).Take(length).ToList();
            }
            else
            {
                count = _dbContext.ScheduleHistories.Where(n => n.JobName == HttpUtility.HtmlDecode(name)).
                  Where(n => n.JobGroup == HttpUtility.HtmlDecode(group)).Count();

                list = _dbContext.ScheduleHistories.Where(n => n.JobName == HttpUtility.HtmlDecode(name)).
                    Where(n => n.JobGroup == HttpUtility.HtmlDecode(group)).OrderByDescending(n => n.BeginTime).Skip(start).Take(length).ToList();
            }


            Dictionary<string, object> dic = new Dictionary<string, object>
            {
                { "recordsTotal",count},
                { "recordsFiltered", count},
                { "list", list }
            };
            return Json(dic);

        }
    }
}