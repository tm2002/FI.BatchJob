﻿using FI.BatchJob.Web.Data;
using FI.BatchJob.Web.Data.Entitities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace FI.BatchJob.Web.Controllers
{

    [Authorize]
    public class MailConfigController : Controller
    {

        private ApplicationDbContext _context;

        public MailConfigController(ApplicationDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// 展示页面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            var config = _context.MailConfigs.FirstOrDefault();

            if (config == null) config = new MailConfig();

            return View(config);
        }



        /// <summary>
        /// 修改mail 配置
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Save(MailConfig config)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("所有字段是必填的");
            }

            if (config.ID > 0)
            {
                _context.MailConfigs.Update(config);
            }
            else
            {
                _context.MailConfigs.Add(config);
            }
            await _context.SaveChangesAsync();

            return Json(new { success = true, message = "修改成功" });
        }



    }
}

