﻿using FI.BatchJob.Web.Data;
using FI.BatchJob.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data;
using System.Linq;
namespace FI.BatchJob.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ApplicationDbContext _repository;


        public HomeController(ApplicationDbContext repository)
        {
            _repository = repository;

        }
        public IActionResult Index()
        {
            using (var command = _repository.Database.GetDbConnection().CreateCommand())
            {
                using (var connect = command.Connection)
                {
                    connect.Open();
                    command.CommandText = "select count(1) from QRTZ_JOB_DETAILS";
                    command.CommandType = CommandType.Text;
                    ViewBag.TaskCount = Convert.ToInt32(command.ExecuteScalar());
                }
            }

            ViewBag.HistoryCount = _repository.ScheduleHistories.Count();
            ViewBag.UserCount = _repository.Users.Count();
            ViewBag.EmailQueueCount = EmailQueue.Instance.EmailCount();

            return View();
        }





    }
}
