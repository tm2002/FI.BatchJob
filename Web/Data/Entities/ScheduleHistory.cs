﻿using FI.BatchJob.Web.Data.Entitities;
using System;
using System.ComponentModel.DataAnnotations;
using static FI.BatchJob.Web.Data.Entitities.EnumType;

namespace FI.BatchJob.Web.Data.Entities
{
    /// <summary>
    /// 任务调度历史记录
    /// </summary>
    public class ScheduleHistory
    {

        [Key]

        public string ID { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        [StringLength(200)]
        public string JobName { get; set; }
        /// <summary>
        /// 任务分组
        /// </summary>
        [StringLength(200)]
        public string JobGroup { get; set; }
        /// <summary>
        /// 任务描述
        /// </summary>
        [StringLength(500)]
        public string JobDescription { get; set; }
        /// <summary>
        /// 触发类型
        /// </summary>
        public TriggerType TriggerType { get; set; }
        /// <summary>
        /// 执行周期表达式
        /// </summary>
        [StringLength(200)]
        public string Cron { get; set; }
        /// <summary>
        /// 重复次数
        /// </summary>
        public int RepeatTimes { get; set; }
        /// <summary>
        /// 执行间隔时间, 秒为单位
        /// </summary>
        public int Interval { get; set; }

        /// <summary>
        /// 任务运行状态
        /// </summary>
        public JobRunStatus JobStatus { get; set; }

        /// 上次执行时间
        /// </summary>
        public DateTime? PreviousFireTime { get; set; }
        /// <summary>
        /// 下次执行时间
        /// </summary>
        public DateTime? NextFireTime { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 停止时间
        /// </summary>
        public DateTime? EndTime { get; set; }


        /// <summary>
        /// 参数，json格式
        /// </summary>

        [StringLength(500)]
        public string Params { get; set; }
        /// <summary>
        /// 任务所在DLL对应的程序集名称
        /// </summary>
        [StringLength(200)]
        public string AssemblyName { get; set; }
        /// <summary>
        /// 任务所在类
        /// </summary>
        [StringLength(200)]
        public string ClassName { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        [StringLength(500)]
        public string Exception { get; set; }


        public DateTime CreatedTime { get; set; }

        /// <summary>
        /// 任务备注
        /// </summary>
        [StringLength(500)]
        public string Notes { get; set; }

        public ScheduleHistory CopyFrom(Schedule entity)
        {
            this.JobName = entity.JobName;
            this.JobGroup = entity.JobGroup;
            this.Interval = entity.Interval;
            this.JobDescription = entity.JobDescription;
            this.TriggerType = entity.TriggerType;
            this.Cron = entity.Cron;

            this.ClassName = entity.ClassName;
            this.AssemblyName = entity.AssemblyName;
            this.Params = entity.Params;

            this.RepeatTimes = entity.RepeatTimes;

            this.PreviousFireTime = entity.PreviousFireTime;
            this.NextFireTime = entity.NextFireTime;
            this.BeginTime = entity.BeginTime;
            this.EndTime = entity.EndTime;

            return this;
        }

    }
}
