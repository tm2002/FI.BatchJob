﻿using System.ComponentModel.DataAnnotations;

namespace FI.BatchJob.Web.Data.Entitities
{
    /// <summary>
	/// 枚举类型
	/// </summary>
	public class EnumType
    {

        public enum JobRunStatus
        {
            Unstart = 0,
            Running = 1,
            Completed = 2,
            CompletedWithException = 3

        }

        public enum TriggerType
        {
            CRON = 1,
            SIMPLE = 2,
        }
        public enum Status
        {
            [Display(Name = "有效")]
            Active = 1,
            [Display(Name = "失效")]
            Inactive = 0,

        }
    }
}
