﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FI.BatchJob.Web.Data.Entitities
{
    public class ExceptionMail
    {

        /// <summary>
        /// 主题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }





    }

    public class MailConfig
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// 发件邮箱
        /// </summary>
        [Required(ErrorMessage = "发件人是必须的")]
        public string MailFrom { get; set; }

        /// <summary>
        /// 邮箱用户
        /// </summary>
        [Required(ErrorMessage = "用户名是必须的")]
        public string MailUser { get; set; }
        /// <summary>
        /// 邮箱密码
        /// </summary>
        [Required(ErrorMessage = "密码是必须的")]
        [DataType(DataType.Password)]
        public string MailPwd { get; set; }

        /// <summary>
        /// 发件服务器
        /// </summary>
        [Required(ErrorMessage = "邮件主机是必须的")]
        public string MailHost { get; set; }

        /// <summary>
        /// 收件邮箱
        /// </summary>
        [Required(ErrorMessage = "收件人是必须的")]
        public string MailTo { get; set; }
    }
}
