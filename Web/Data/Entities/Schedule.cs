﻿using Quartz;
using System;
using static FI.BatchJob.Web.Data.Entitities.EnumType;

namespace FI.BatchJob.Web.Data.Entitities
{
    /// <summary>
    /// 任务调度实体
    /// </summary>
    public class Schedule
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string JobName { get; set; }
        /// <summary>
        /// 任务分组
        /// </summary>
        public string JobGroup { get; set; }

        /// <summary>
        /// 任务描述
        /// </summary>
        public string JobDescription { get; set; }

        /// <summary>
        /// TriggerType:Simple or Cron
        /// </summary>
        public TriggerType TriggerType { get; set; }
        /// <summary>
        /// 执行周期表达式
        /// </summary>
        public string Cron { get; set; }
        /// <summary>
        /// 执行次数
        /// </summary>
        public int RepeatTimes { get; set; }
        /// <summary>
        /// 执行间隔时间, 秒为单位
        /// </summary>
        public int Interval { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 停止时间
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// Tigger状态
        /// </summary>
        public TriggerState TriggerState { get; set; }

        /// <summary>
        /// 任务运行状态
        /// </summary>
        public JobRunStatus LastRunStatus { get; set; }

        /// 上次执行时间
        /// </summary>
        public DateTime? PreviousFireTime { get; set; }
        /// <summary>
        /// 下次执行时间
        /// </summary>
        public DateTime? NextFireTime { get; set; }

        /// <summary>
        /// 参数，json格式
        /// </summary>
        public string Params { get; set; }
        /// <summary>
        /// 任务所在DLL对应的程序集名称
        /// </summary>
        public string AssemblyName { get; set; }
        /// <summary>
        /// 任务所在类
        /// </summary>
        public string ClassName { get; set; }

    }

}
