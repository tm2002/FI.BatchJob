﻿using FI.BatchJob.Web.Data.Entities;
using FI.BatchJob.Web.Data.Entitities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FI.BatchJob.Web.Data
{

    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        public DbSet<ScheduleHistory> ScheduleHistories { get; set; }
        //public DbSet<ExceptionMail> ExceptionMails { get; set; }

        public DbSet<MailConfig> MailConfigs { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<ScheduleHistory>().ToTable("QRTZ_SCHEDULE_HISTORY");
            //builder.Entity<ExceptionMail>().ToTable("QRTZ_EXCEPTION_MAIL"); 
            builder.Entity<MailConfig>().ToTable("QRTZ_MAIL_CONFIG");
        }
    }
}
