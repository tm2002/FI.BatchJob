﻿

using FI.BatchJob.Web.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data;
using System.IO;

namespace FI.BatchJob.Web.Extensions
{
    public class InitDataTool
    {
        private RoleManager<IdentityRole> _roleManager;
        private UserManager<IdentityUser> _userManager;
        private ApplicationDbContext _dbContext;

        public InitDataTool(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager, ApplicationDbContext applicationDbContext)
        {
            _userManager = userManager;
            _userManager.Options.User.RequireUniqueEmail = false;
            _roleManager = roleManager;
            _dbContext = applicationDbContext;
        }

        public void EnsureSeedData()
        {
            string username = "admin";
            string password = "123456";
            string roleNameAdmin = "Admin";
            string roleNameCustomer = "Customer";

            var r = _roleManager.FindByNameAsync(roleNameAdmin).Result;
            var rc = _roleManager.FindByNameAsync(roleNameCustomer).Result;
            //create admin role 
            if (r == null)
            {
                var res = _roleManager.CreateAsync(new IdentityRole(roleNameAdmin)).Result;

                if (!res.Succeeded)
                {
                    throw new Exception("create admin role failed");
                }
            }
            //create admin role 
            if (rc == null)
            {
                var res = _roleManager.CreateAsync(new IdentityRole(roleNameCustomer)).Result;

                if (!res.Succeeded)
                {
                    throw new Exception("create customer role failed");
                }
            }
            //create admin account
            if (_userManager.FindByNameAsync(username).Result == null)
            {
                IdentityUser administrator = new IdentityUser()
                {
                    UserName = username,
                    LockoutEnabled = true,
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true
                };

                // var userName = _userManager.GetUserNameAsync(administrator).Result;

                var u = _userManager.CreateAsync(administrator, password).Result;

                if (u.Succeeded)
                {
                    var r1 = _userManager.AddToRoleAsync(administrator, roleNameAdmin).Result;
                    if (!r1.Succeeded)
                    {
                        throw new Exception("adding role for user failed");
                    }
                }
                else
                {
                    throw new Exception("adding user failed");
                }
            }

            if (File.Exists("./tables_sqlite.sql"))
            {
                //create quartz table 
                string sql = File.ReadAllText("./tables_sqlite.sql");

                using (var command = _dbContext.Database.GetDbConnection().CreateCommand())
                {
                    using (var connect = command.Connection)
                    {
                        connect.Open();
                        command.CommandText = sql;
                        command.CommandType = CommandType.Text;
                        command.ExecuteNonQuery();
                    }
                }
            }
            else
            {
                throw new Exception("cannot find the 'tables_sqlite.sql' to initilize table schema of quartz.net.");
            }
        }
    }
}

