﻿namespace FI.BatchJob.Web.Constants
{
    public class Constant
    {

        /// <summary>
        ///  邮件
        /// </summary>
        public static readonly string MAIL_MESSAGE = "MAIL_MESSAGE";

        /// 异常 Exception
        /// </summary>
        public static readonly string EXCEPTION = "EXCEPTION";


        public static readonly string RUNNING_PARAMS = "RUNNING_PARAMS";

        public static readonly string ASSEMBLY_NAME = "ASSEMBLY_NAME";

        public static readonly string CLASS_NAME = "CLASS_NAME";

        public static readonly string ORIGINAL_SCHEDULE = "ORIGINAL_SCHEDULE";

        public static readonly string SCHEDULE_HISTORY = "SCHEDULE_HISTORY";

        public static readonly string JOB_RUN_STATUS = "JOB_RUN_STATUS";

        public static readonly string SCHEDULE_HISTORY_ID = "SCHEDULE_HISTORY_ID";

        public static readonly string NOTES = "NOTES";
    }
}
