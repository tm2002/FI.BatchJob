FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY . .
WORKDIR "/src/Web"
RUN dotnet build "FI.BatchJob.Web.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "FI.BatchJob.Web.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .

RUN cp /usr/share/zoneinfo/Asia/Shanghai /usr/share/zoneinfo/Asia/Beijing
 

RUN unlink /etc/localtime \
&& echo 'Asia/Shanghai' > /etc/timezone \
&& ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime;

ENTRYPOINT ["dotnet", "FI.BatchJob.Web.dll"]