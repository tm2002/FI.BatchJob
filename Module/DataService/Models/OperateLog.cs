﻿using System;

namespace FI.BatchJob.DataService.Models
{
    /// <summary>
    /// 操作日志表
    /// </summary>

    public class OperateLog
    {
        /// <summary>
        /// 日志编号
        /// </summary>
       
        public int ID { get; set; }
        /// <summary>
        /// 操作表名称
        /// </summary>

        public string TABLENAME { get; set; }
        /// <summary>
        /// 描述

        public string DESCRIBE { get; set; }
        /// <summary>
        /// 日志类型(0：正常，1：异常)
        /// </summary>
        public int TYPE { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime CREATETIME { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UPDATETIME { get; set; }
    }
}
