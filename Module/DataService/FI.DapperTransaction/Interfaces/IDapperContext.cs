﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FI.Dapper.Transaction
{
    public interface IDapperContext : IDisposable
    {
        IDbConnection Connection { get; }
        IDbTransaction Transaction { get; }
    }
}
