﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FI.Dapper.Transaction
{
    public static class ConfigManager
    {
        public static IConfiguration Configuration;
        static ConfigManager()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            // 根据环境变量添加相应的 JSON 配置文件
            if (!string.IsNullOrEmpty(env))
            {
                builder = builder.AddJsonFile($"appsettings.{env}.json", optional: true, reloadOnChange: true);
            }
            Configuration = builder.Build();
        }

        public static string Get(string name)
        {
            string appSettings = Configuration[name];
            return appSettings;
        }


        
    }
}
