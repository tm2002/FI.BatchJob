﻿using FI.Dapper.Transaction;

namespace FI.Dapper.Interfaces
{
    public class FactoryRepository
    {
        public IRepository CreateRepository(IDapperContext context)  
        {
            return new Repository(context);

        }

    }
}