﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace FI.Dapper.Interfaces
{
    public interface IRepository
    {
     
        GridReader QueryMultiple(string sql, object param = null, int? commandTimeout = null, CommandType text = CommandType.Text);
        Task<GridReader> QueryMultipleAsync(string sql, object param = null, int? commandTimeout = null, CommandType text = CommandType.Text);
        int Execute(string sql, object param = null, int? commandTimeout = null, CommandType text = CommandType.Text);
        Task<int> ExecuteAsync(string sql, object param = null, int? commandTimeout = null, CommandType text = CommandType.Text);
        T ExecuteScalar<T>(string sql, object param = null, int? commandTimeout = null, CommandType text = CommandType.Text);
        Task<T> ExecuteScalarAsync<T>(string sql, object param = null, int? commandTimeout = null, CommandType text = CommandType.Text);
        IEnumerable<T> Query<T>(string sql, object param = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null);
        Task<IEnumerable<T>> QueryAsync<T>(string sql, object param = null, int? commandTimeout = null, CommandType? commandType = null);
        IEnumerable<dynamic> Query(string sql, object param = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null);
        Task<IEnumerable<dynamic>> QueryAsync(string sql, object param = null, int? commandTimeout = null, CommandType? commandType = null);
        IDbConnection Connection { get; }
        IDbTransaction Transaction { get; }

        int ExecuteProc(string procedureName, DynamicParameters dynamicParameters, int? commandTimeout = null);

        int ExecuteSQL(string sql, DynamicParameters dynamicParameters, int? commandTimeout = null);

        T ExecuteScalerBySQL<T>(string sql, DynamicParameters dynamicParameters, int? commandTimeout = null);

        T GetByProc<T>(string procedureName, DynamicParameters dynamicParameters, int? commandTimeout = null) where T : class;

        T GetBySQL<T>(string sql, DynamicParameters dynamicParameters, int? commandTimeout = null) where T : class;

        IEnumerable<T> GetListBySQL<T>(string sql, DynamicParameters dynamicParameters, int? commandTimeout = null) where T : class;

        IEnumerable<T> GetListByProc<T>(string procedureName, DynamicParameters dynamicParameters, int? commandTimeout = null) where T : class;



    }
}