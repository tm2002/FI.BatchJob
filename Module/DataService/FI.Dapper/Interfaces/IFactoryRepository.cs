﻿using FI.Dapper.Transaction;

namespace FI.Dapper.Interfaces
{
    public interface IFactoryRepository
    {
        IRepository CreateRepository<T>(IDapperContext context) where T : class;
          
    }
}