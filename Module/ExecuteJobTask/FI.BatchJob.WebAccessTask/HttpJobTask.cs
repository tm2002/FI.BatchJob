﻿
using Quartz;

using System;
using System.Threading.Tasks;
using FI.BatchJob.Common;

namespace FI.BatchJob.WebAccessTask
{
    [DisallowConcurrentExecution]
    public class HttpJobTask : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {

            var jobGroup = context.JobDetail.Key.Group;
            var jobName = context.JobDetail.Key.Name;
            try
            {
                var url = context.JobDetail.JobDataMap.GetString(Constants.REMOTE_URL);

                var item = new HttpItem
                {
                    URL = url,
                    Method = "post"
                };
                var result = new HttpHelper().GetHtml(item).Html;

                await Console.Out.WriteLineAsync(string.Format("任务分组：{0}, 任务名称：{1}, 请求返回信息：{2}", jobGroup, jobName, result));

                context.JobDetail.JobDataMap.Put(Constants.NOTES, "URL:" + url + " has been reached successfully, and the got result is " + result);

                await Task.CompletedTask;
            }
            catch (Exception ex)
            {
                await Task.FromException(ex);

            }
        }
    }
}
