﻿using Quartz;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace FI.BatchJob.SimpleTask
{
    [DisallowConcurrentExecution]
    public class HellowWorldTask : IJob
    {
        public HellowWorldTask()
        {

        }
        public async Task Execute(IJobExecutionContext context)
        {

            int i = 0;

            while (true)
            {
                try
                {
                    if (i > 6) break;

                    if (new Random().Next(1, 100) % 5 == 0)
                    {
                        throw new Exception("this is a test.");
                    }

                    Thread.Sleep(10000);

                    i++;

                }
                catch
                {
                    context.Result = $"成功处理{i}条";

                    throw;

                }


            }

            context.Result = $"成功处理{i}条";

            await Task.CompletedTask;


        }
    }
}
